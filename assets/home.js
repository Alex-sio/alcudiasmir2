/* Fait Par Alexis Lesnes */
const nav = document.querySelector('.nav');
const navToggle = document.querySelector('.btn_menu');

if(navToggle !== null || nav !== null){

    navToggle.addEventListener("click", () => {

        const visibility = nav.getAttribute('data-visible');
        
        if(visibility === "false"){
            nav.setAttribute("data-visible", true);
            navToggle.setAttribute("aria-expended", true);
        }else if(visibility === "true"){
            nav.setAttribute("data-visible", false);
            navToggle.setAttribute("aria-expended", false);
        }
    
    });

}

