/* Fait par Alexis Lesnes */
console.log(screen.width+"*"+screen.height);

/* Side Bar Admin */
const body = document.querySelector(".body_admin");
const sidebar = body.querySelector(".sidebar");
const toggle = body.querySelector(".toggle");
const modeSwitch = body.querySelector(".toggle-switch");
const modeText = body.querySelector(".mode-text");

if (body !== null){

    modeSwitch.addEventListener("click", () => {
        body.classList.toggle("dark");

        if(body.classList.contains("dark")){
            modeText.innerHTML = "Light Mode";
        }else{
            modeText.innerHTML = "Dark Mode";
        }
    });

    toggle.addEventListener("click", () => {
        sidebar.classList.toggle("close");
    });

    window.addEventListener("load", () =>{

        const tabs = document.querySelectorAll(".tab");
        const contents = document.querySelectorAll(".content");
        let active = document.querySelector(".active");
        let target = active.dataset.target;
    
        if(tabs !== null){
            
        show();
    
        document.getElementById(target).classList.add("show");
        
        }
    
    
        function show(){
            for(let tab of tabs){
                this.removeEventListener("click", tabclick);
                if(tab !== active){
                    tab.addEventListener("click", tabclick);
                    tab.classList.remove("active");
                }
            }
    
            for(let content of contents){
                content.classList.remove("show");
            }
        }
    
        function tabclick(){
            target = this.dataset.target;
            active = this;
            this.classList.add("active");
            this.removeEventListener("click", tabclick);
            show();
            document.getElementById(target).classList.add("show");
        }
    });

}

