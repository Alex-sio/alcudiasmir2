<?php

namespace App\Controller;

use App\Entity\Member;
use App\Entity\News;
use App\Entity\Page;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use FOS\CKEditorBundle\Form\Type\CKEditorType;

class AdminController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){
            return $this->render('admin/index.html.twig', [
                'page_title' => 'Alcudia Smir | Administration',
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }

    }


    /**
     * @Route("/admin/membres", name="membres")
     */
    public function membres(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
            $UserTab = $doctrine->getRepository(Member::class)->findAll();

            return $this->render('admin/membres.html.twig', [
                'page_title' => 'Alcudia Smir | Gestion des Membres',
                'alpha' => $alphabet,
                'usertab' => $UserTab, 
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/membres/{id}", name="membre")
     */
    public function membre(int $id, Request $request, ManagerRegistry $doctrine): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $user = $doctrine->getRepository(Member::class)->findOneBy(array('id' => $id));

            if (!$user) {
                throw $this->createNotFoundException(
                    'Aucun utilisateur trouvée pour l\'id '.$id
                );
            }

            $member = new Member();
            $member->setimg($user->getimg());
            $member->setname($user->getname());
            $member->setlastname($user->getlastname());
            $member->setemail($user->getemail());
            $member->setlevel($user->getlevel());
            $member->setPasswd($user->getPasswd());
            $member->setLandTitle($user->getLandTitle());
            $member->setphone($user->getphone());
            $member->setAddress($user->getAddress());
            $member->setadditional($user->getadditional());
            $member->setcity($user->getcity());
            $member->setPostalCode($user->getPostalCode());
            $member->setcountry($user->getcountry());

            $form = $this->createFormBuilder($member)
            ->add('img', TextType::class, ['required' => false])
            ->add('name', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', TextType::class)
            ->add('level', TextType::class)
            ->add('passwd', TextType::class)
            ->add('land_title', TextType::class)
            ->add('phone', TextType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('additional', TextType::class, ['required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('postal_code', TextType::class, ['required' => false])
            ->add('country', TextType::class, ['required' => false])
            ->add('delete', SubmitType::class, ['label' => 'Supprimer'])
            ->add('modify', SubmitType::class, ['label' => 'Modifier'])
            ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if($form->get('delete')->isClicked()) {
                    
                    $entityManager = $doctrine->getManager();
                    $deleteUser = $entityManager->getRepository(Member::class)->find($id);

                    $entityManager->remove($deleteUser);
                    $entityManager->flush();

                    return $this->redirectToRoute('membres');

                }elseif($form->get('modify')->isClicked()) {

                    $userData = $form->getData();

                    $entityManager = $doctrine->getManager();
                    $updateUser = $entityManager->getRepository(Member::class)->find($id);

                    $updateUser->setimg($userData->getimg());
                    $updateUser->setname($userData->getname());
                    $updateUser->setlastname($userData->getlastname());
                    $updateUser->setemail($userData->getemail());
                    $updateUser->setlevel($userData->getlevel());
                    $updateUser->setPasswd($userData->getPasswd());
                    $updateUser->setLandTitle($userData->getLandTitle());
                    $updateUser->setphone($userData->getphone());
                    $updateUser->setAddress($userData->getAddress());
                    $updateUser->setadditional($userData->getadditional());
                    $updateUser->setcity($userData->getcity());
                    $updateUser->setPostalCode($userData->getPostalCode());
                    $updateUser->setcountry($userData->getcountry());
                    
                    $entityManager->flush();

                    return $this->redirectToRoute('membre', ['id' => $id]);
                }
            }

            return $this->render('admin/membregestion.html.twig', [
                'page_title' => 'Alcudia Smir | Membre',
                'user' => $user,
                'formUserModify' => $form->createView(),
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/carousel", name="acarousel")
     */
    public function admincarousel(): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            return $this->render('admin/carousel.html.twig', [
                'page_title' => 'Alcudia Smir | Gestion des Carousel',
                'level' => $level,
            ]);

        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/commentaires", name="acommentaires")
     */
    public function admincommentaires(): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            return $this->render('admin/commentaires.html.twig', [
                'page_title' => 'Alcudia Smir | Gestion des Commentaires',
                'level' => $level,
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/deletepage/{id}", name="pagedelete")
     */
    public function adminpagedelete(int $id, ManagerRegistry $doctrine): Response
    {

        $entityManager = $doctrine->getManager();
        $deletePage = $entityManager->getRepository(Page::class)->find($id);

        $entityManager->remove($deletePage);
        $entityManager->flush();

        return $this->redirectToRoute('modifpages');

    }

    /**
     * @Route("/admin/pages/{id}", name="pagemodif")
     */
    public function modifpages(int $id, ManagerRegistry $doctrine, Request $request): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $lang="fr";

        if($level === 5){

            $pageM = $doctrine->getRepository(Page::class)->findOneBy(array('id' => $id));

            $page = new Page();
            $page->setTitle($pageM->getTitle());
            $page->setFr($pageM->getFr());
            $page->setEn($pageM->getEn());
            $page->setMa($pageM->getMa());

            switch($lang){
                case "fr":
                    $form = $this->createFormBuilder($page)
                    ->add('title', TextType::class)
                    ->add('fr', CKEditorType::class, array(
                        'config' => array(
                            'uiColor' => '#ffffff',
                            'toolbar' => 'standard')))
                    ->add('save', SubmitType::class, ['label' => 'Modifier'])
                    ->getForm();
                    break;
                case "en":
                    $form = $this->createFormBuilder($page)
                    ->add('title', TextType::class)
                    ->add('en', CKEditorType::class, array(
                        'config' => array(
                            'uiColor' => '#ffffff',
                            'toolbar' => 'standard')))
                    ->add('save', SubmitType::class, ['label' => 'Modifier'])
                    ->getForm();
                    break;
                case "ma":
                    $form = $this->createFormBuilder($page)
                    ->add('title', TextType::class)
                    ->add('ma', CKEditorType::class, array(
                        'config' => array(
                            'uiColor' => '#ffffff',
                            'toolbar' => 'standard')))
                    ->add('save', SubmitType::class, ['label' => 'Modifier'])
                    ->getForm();
                    break;
                default:
                    die('erreur de langue');
                    break;
            }

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $ModifP = $form->getData();

                $entityManager = $doctrine->getManager();
                $updatePage = $entityManager->getRepository(Page::class)->find($id);

                $updatePage->setTitle($ModifP->getTitle());
                $updatePage->setFr($ModifP->getFr());
                $updatePage->setEn($ModifP->getEn());
                $updatePage->setMa($ModifP->getMa());

                $entityManager->flush();

                return $this->redirectToRoute('pagemodif', ['id' => $id]);

            }


            return $this->render('admin/pagesmodif.html.twig', [
                'page_title' => 'Alcudia Smir | Modification de pages',
                'level' => $level,
                'formModifPage' => $form->createView(),
            ]);

        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/pages", name="modifpages")
     */
    public function madminpages(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $tabPage = $doctrine->getRepository(Page::class)->findAll();

            return $this->render('admin/modifpages.html.twig', [
                'page_title' => 'Alcudia Smir | Gestion des Pages',
                'level' => $level,
                'tabPage' => $tabPage,
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/deletenews/{id}", name="newsdelete")
     */
    public function adminnewsdelete(int $id, ManagerRegistry $doctrine): Response
    {

        $entityManager = $doctrine->getManager();
        $deleteNews = $entityManager->getRepository(News::class)->find($id);

        $entityManager->remove($deleteNews);
        $entityManager->flush();

        return $this->redirectToRoute('modifnews');

    }

    /**
     * @Route("/admin/modifnews/{id}", name="newsmodif")
     */
    public function adminnewsmodif(int $id, ManagerRegistry $doctrine, Request $request): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $news = $doctrine->getRepository(News::class)->findOneBy(array('id' => $id));

            $newsM = new News();
            $newsM->setTitle($news->getTitle());
            $newsM->setAlias($news->getAlias());
            $newsM->setText($news->getText());
            $newsM->setResume($news->getResume());

            $form = $this->createFormBuilder($newsM)
            ->add('title', TextType::class)
            ->add('alias', TextType::class)
            ->add('text', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    'toolbar' => 'standard')))
            ->add('resume', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    'toolbar' => 'standard')))
            ->add('save', SubmitType::class, ['label' => 'Modifier'])
            ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $ModifN = $form->getData();

                $entityManager = $doctrine->getManager();
                $updateNews = $entityManager->getRepository(News::class)->find($id);

                $updateNews->setTitle($ModifN->getTitle());
                $updateNews->setAlias($ModifN->getAlias());
                $updateNews->setText($ModifN->getText());
                $updateNews->setResume($ModifN->getResume());

                $entityManager->flush();

                return $this->redirectToRoute('newsmodif', ['id' => $id]);

            }

            return $this->render('admin/newsmodif.html.twig', [
                'page_title' => 'Alcudia Smir | Modification des News',
                'level' => $level,
                'news' => $news,
                'formModifNews' => $form->createView(),
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/modifnews", name="modifnews")
     */
    public function adminmodifnews(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $tabnews = $doctrine->getRepository(News::class)->findAll();

            return $this->render('admin/modifnews.html.twig', [
                'page_title' => 'Alcudia Smir | Modification des news',
                'level' => $level,
                'tabnews' => $tabnews,
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/admin/ajoutnews", name="ajoutnews")
     */
    public function adminajoutnews(Request $request, ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        if($level === 5){

            $form = $this->createFormBuilder()
            ->add('title', TextType::class)
            ->add('alias', TextType::class)
            ->add('text', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    'toolbar' => 'standard')))
            ->add('resume', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    'toolbar' => 'standard')))
            ->add('id', HiddenType::class, ['data' => $session->get('id')])
            ->add('save', SubmitType::class, ['label' => 'Ajouter'])
            ->getForm();

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {

                $newsAdd = $form->getData();
                $entityManager = $doctrine->getManager();

                $newsUser = $doctrine->getRepository(Member::class)->findOneBy(array('id' => $newsAdd['id']));

                $AddNews = new News();
                $AddNews->setTitle($newsAdd['title']);
                $AddNews->setText($newsAdd['text']);
                $AddNews->setDate(new \DateTime());
                $AddNews->setResume($newsAdd['resume']);
                $AddNews->setAlias($newsAdd['alias']);
                $AddNews->setMember($newsUser);
                $AddNews->setImg('test.png');

                $entityManager->persist($AddNews);
                $entityManager->flush();

                return $this->redirectToRoute('ajoutnews');

            }

            return $this->render('admin/ajoutnews.html.twig', [
                'page_title' => 'Alcudia Smir | Ajout des News',
                'level' => $level,
                'formAjout' => $form->createView(),
            ]);
        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }
}
