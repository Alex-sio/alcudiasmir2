<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Member;
use App\Entity\Carousel;
use App\Entity\Page;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;

class HomeController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(ManagerRegistry $doctrine): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');

        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="accueil";

        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        $allnews = $doctrine->getRepository(News::class)->findAll();
        $tabMember = array();
        foreach($allnews as $news ){
            $memberNews = $doctrine->getRepository(Member::class)->findOneBy(array('id' => $news->getMember()));
            array_push($tabMember, $memberNews);
        }
        
        return $this->render('home/index.html.twig', [
            'page_title' => 'Alcudia Smir | Accueil',
            'level' => $level,
            'Auth' => $isAuth,
            'allnews' => $allnews,
            'memberNews' => $tabMember,
            'carousel' => $carousel,
            'active' => $active,
        ]);
    }

    /**
     * @Route("/acceuil", name="accueil")
     */
    public function accueil(ManagerRegistry $doctrine): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');

        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="accueil";

        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        $allnews = $doctrine->getRepository(News::class)->findAll();
        $tabMember = array();
        foreach($allnews as $news ){
            $memberNews = $doctrine->getRepository(Member::class)->findOneBy(array('id' => $news->getMember()));
            array_push($tabMember, $memberNews);
        }
        
        return $this->render('home/index.html.twig', [
            'page_title' => 'Alcudia Smir | Accueil',
            'level' => $level,
            'Auth' => $isAuth,
            'allnews' => $allnews,
            'memberNews' => $tabMember,
            'carousel' => $carousel,
            'active' => $active,
        ]);
    }

    /**
     * @Route("/news/{id}", name="news")
     */
    public function news(int $id, ManagerRegistry $doctrine): Response
    {
        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="accueil";

        $news = $doctrine->getRepository(News::class)->findOneBy(array('id' => $id));
        $memberN = $doctrine->getRepository(Member::class)->findOneBy(array('id' => $news->getMember()));
        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        return $this->render('home/news.html.twig', [
            'page_title' => 'Alcudia Smir | '.$news->gettitle(),
            'level' => $level,
            'Auth' => $isAuth,
            'news' => $news,
            'member' => $memberN,
            'active' => $active,
            'carousel' => $carousel,
        ]);
    }

    /**
     * @Route("/conseil_syndical", name="conseilsyndical")
     */
    public function conseilsyndical(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $page_csyndical = $doctrine->getRepository(Page::class)->findOneBy(array('title' => "Conseil Syndical"));



        $active="csyndical";
        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        return $this->render('home/conseilsyndical.html.twig', [
            'page_title' => 'Alcudia Smir | Conseil Syndical',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
            'carousel' => $carousel,
            'csyndical' => $page_csyndical,
        ]);
    }

    /**
     * @Route("/divers", name="divers")
     */
    public function divers(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $page_divers = $doctrine->getRepository(Page::class)->findOneBy(array('title' => "Divers"));

        $active="divers";
        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        return $this->render('home/divers.html.twig', [
            'page_title' => 'Alcudia Smir | Divers',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
            'carousel' => $carousel,
            'divers' => $page_divers,
        ]);
    }

    /**
     * @Route("/syndic", name="syndic")
     */
    public function syndic(ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $page_syndic = $doctrine->getRepository(Page::class)->findOneBy(array('title' => "Le Syndic"));

        $active="syndic";
        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        return $this->render('home/syndic.html.twig', [
            'page_title' => 'Alcudia Smir | Le Syndic',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
            'carousel' =>$carousel,
            'syndic' => $page_syndic,
        ]);
    }

    /**
     * @Route("/forum", name="forum")
     */
    public function forum(): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="forum";

        return $this->render('home/forum.html.twig', [
            'page_title' => 'Alcudia Smir | Forum',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
        ]);
    }
}
