<?php

namespace App\Controller;

use App\Entity\Member;
use App\Entity\Carousel;
use Doctrine\Persistence\ManagerRegistry;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;

class UserController extends AbstractController
{

    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @Route("/compte", name="user")
     */
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="user";

        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        $form = $this->createFormBuilder()
            ->add('login', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, ['label' => 'Connexion'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userCo = $form->getData();

            $emailUser = $this->getDoctrine()->getRepository(Member::class)->findOneBy(array('email' => $userCo['login']));

            if(password_verify($userCo['password'], $emailUser->getPasswd()) && $emailUser->getActive() == 1){
                $session->set('level', $emailUser->getlevel());
                $session->set('lastname', $emailUser->getlastname());
                $session->set('name', $emailUser->getname());
                $session->set('email', $emailUser->getemail());
                $session->set('country', $emailUser->getcountry());
                $session->set('id', $emailUser->getid());

                //json_encode($_SESSION);

                return $this->redirectToRoute('connect_user');
            }else{
                return $this->render('error404.html.twig', [
                    'page_title' => 'Alcudia Smir | Erreur 404',
                ]);
            }
        }

        $formInsc = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('save', SubmitType::class, ['label' => 'Inscription'])
            ->getForm();
        
        $formInsc->handleRequest($request);
        if ($formInsc->isSubmitted() && $formInsc->isValid()) {

            return $this->redirectToRoute('inscription_user');
        }

        return $this->render('user/index.html.twig', [
            'page_title' => 'Alcudia Smir | User',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
            'carousel' => $carousel,
            'formCo' => $form->createView(),
            'formInsc' => $formInsc->createView(),
        ]);
    }

    /**
     * @Route("/moncompte", name="connect_user")
     */
    public function connexion(Request $request, ManagerRegistry $doctrine): Response
    {
        $session = $this->requestStack->getSession();
        
        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null && $session->get('email') != null){
            $level = $session->get('level');
            if(!$level){
                $level = 0;
            }

            $isAuth = true;

            $active="user";
            $carousel = $doctrine->getRepository(Carousel::class)->findAll();

            $user = $this->getDoctrine()->getRepository(Member::class)->findOneBy(array('email' => $session->get('email'), 'lastname' => $session->get('lastname'), 'name' => $session->get('name')));
            
            $form = $this->createFormBuilder()
                ->add('save', SubmitType::class, ['label' => 'Deconnexion'])
                ->getForm();

                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {

                    return $this->redirectToRoute('deco');
                }
            
            return $this->render('user/moncompte.html.twig', [
                'page_title' => 'Alcudia Smir | Mon Compte',
                'level' => $level,
                'user' => $user,
                'Auth' => $isAuth,
                'active' => $active,
                'carousel' => $carousel,
                'formDeCo' => $form->createView(),
            ]);

        }else{
            return $this->render('error404.html.twig', [
                'page_title' => 'Alcudia Smir | Erreur 404',
            ]);
        }
    }

    /**
     * @Route("/deconnexion", name="deco")
     */
    public function deconnexion(Request $request): Response
    {
        $session = $this->requestStack->getSession();

        if($session->get('lastname') != null && $session->get('name') != null && $session->get('email') != null){
            $level = $session->get('level');
            if(!$level){
                $level = 0;
            }

            if($level == 5)
            {
                $this->requestStack->getCurrentRequest()->getSession()->clear();
                return $this->redirectToRoute('user');
            }
        }
    }

    /**
     * @Route("/inscription", name="inscription_user")
     */
    public function inscription(Request $request, ManagerRegistry $doctrine): Response
    {

        $session = $this->requestStack->getSession();
        $level = $session->get('level');
        if(!$level){
            $level = 0;
        }

        $isAuth = false;
        if($session->get('lastname') != null && $session->get('name') != null){
            $isAuth = true;
        }

        $active="user";
        $carousel = $doctrine->getRepository(Carousel::class)->findAll();

        $form = $this->createFormBuilder()
            ->add('img', TextType::class, ['required' => false])
            ->add('name', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', TextType::class)
            ->add('passwd', PasswordType::class)
            ->add('passwd2', PasswordType::class)
            ->add('land_title', TextType::class)
            ->add('phone', TextType::class, ['required' => false])
            ->add('address', TextType::class, ['required' => false])
            ->add('additional', TextType::class, ['required' => false])
            ->add('city', TextType::class, ['required' => false])
            ->add('postal_code', TextType::class, ['required' => false])
            ->add('country', TextType::class, ['required' => false])
            ->add('save', SubmitType::class, ['label' => 'Enregistrer'])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $entityManager = $doctrine->getManager();

            $member = new Member();
            $member->setimg($user['img']);
            $member->setname($user['name']);
            $member->setlastname($user['lastname']);
            $member->setemail($user['email']);
            $member->setlevel(1);
            $member->setPasswd(password_hash($user['passwd'], PASSWORD_DEFAULT));
            $member->setLandTitle($user['land_title']);
            $member->setphone($user['phone']);
            $member->setAddress($user['address']);
            $member->setadditional($user['additional']);
            $member->setcity($user['city']);
            $member->setPostalCode($user['postal_code']);
            $member->setcountry($user['country']);

            $entityManager->persist($member);
            $entityManager->flush();

            $emailUser = $this->getDoctrine()->getRepository(Member::class)->findOneBy(array('email' => $user['email']));

            if(password_verify($user['passwd'], $emailUser->getPasswd())){
                $session->set('level', $emailUser->getlevel());
                $session->set('lastname', $emailUser->getlastname());
                $session->set('name', $emailUser->getname());
                $session->set('email', $emailUser->getemail());
                $session->set('country', $emailUser->getcountry());

                return $this->redirectToRoute('connect_user');
            }else{
                return $this->render('error404.html.twig', [
                    'page_title' => 'Alcudia Smir | Erreur 404',
                ]);
            }
        }

        return $this->render('user/inscription.html.twig', [
            'page_title' => 'Alcudia Smir | Inscription',
            'level' => $level,
            'Auth' => $isAuth,
            'active' => $active,
            'carousel' => $carousel,
            'formIns' => $form->createView(),
        ]);
    }
}
