<div id="top"></div>


<!-- PROJECT LOGO -->
<br />
<!--<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a>-->

  <h3 align="center">Alcudia Smir Website</h3>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">A propos du projet</a>
      <ul>
        <li><a href="#built-with">Composition</a></li>
      </ul>
    </li>
    <li><a href="#usage">Utilisation</a></li>
    <li><a href="#roadmap">Plan du site</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## A propos du projet




<p align="right">(<a href="#top">back to top</a>)</p>



### Composition

Cette section permet de recenser les differents framework et extension constituant le site web

* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Alexis Lesnes - alex13.lesnes@gmail.com

Project Link: [https://gitlab.com/Alex-sio/alcudiasmir2](https://gitlab.com/Alex-sio/alcudiasmir2)

<p align="right">(<a href="#top">back to top</a>)</p>